<?php

if ($razdel->type == 0 || ($razdel->type == 1) && $user['group_access'] > 2) {
    
    $my_last_theme = mysql_fetch_object(mysql_query('SELECT `time` FROM `forum_themes` WHERE `id_user` = '.$user['id'].' ORDER BY `id` DESC'));
    if ($my_last_theme && (time() - $my_last_theme->time < $set['new_t_time'] && $set['new_t_time'] != 0) && $user['group_access'] < 3) {
        
        if ($set['new_t_time'] == 60) {
            $theme_time = 'минуту';
        } elseif ($set['new_t_time'] == 120) {
            $theme_time = '2 минуты';
        } elseif ($set['new_t_time'] == 180) {
            $theme_time = '3 минуты';
        } elseif ($set['new_t_time'] == 300) {
            $theme_time = '5 минут';
        } elseif ($set['new_t_time'] == 600) {
            $theme_time = '10 минут';
        } elseif ($set['new_t_time'] == 1200) {
            $theme_time = '20 минут';
        }
        
        $part = ($set['new_t_time']-(time()-$my_last_theme->time));
        $min = ($part > 60) ? floor(($part / 60) % 60).' минут(ы) '.($part % 60).' секунд(у/ы)' : ' секунд(у/ы)';

        ?>
        <div class="err">
            Тему можно создавать 1 раз в <?= $theme_time ?>.<br />
            Вы сможете создать тему через <?= $min ?>.
        </div>
        
        <?
        include_once '../sys/inc/tfoot.php';
        exit;                
    }
    
    if (isset($_POST['create'])) {
        $name = mysql_real_escape_string(trim($_POST['name']));
        $description = mysql_real_escape_string(trim($_POST['description']));
        $type = ($user['group_access'] > 7 && isset($_POST['type'])) ? 1 : 0;
        if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\,\.\-)]/", $_POST['name'], $m)) {
            ?>
            <div class = 'err'>
                В поле &laquo;Название темы&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?= $m[0]?></span> символы!.
            </div>
            <?            
        } else        
        if (mb_strlen($name) < $set['forum_new_them_name_min_pod'] || mb_strlen($name) > $set['forum_new_them_name_max_pod']) {
            ?>
            <div class = 'err'>В поле &laquo;Название темы&raquo; можно использовать от <?= $set['forum_new_them_name_min_pod'] ?> до <?= $set['forum_new_them_name_max_pod'] ?> символов.</div>
            <?
        } elseif (mb_strlen($description) < 3) {
            ?>
            <div class = 'err'>Слишком короткое содержание темы.</div>
            <?
        } else {
            $_SESSION['create_theme'] = '<div class = "msg">Тема успешно создана.</div>';
            mysql_query('INSERT INTO `forum_themes` SET `id_forum` = '.$forum->id.', `id_razdel` = '.$razdel->id.', `name` = "'.$name.'", `description` = "'.$description.'", `type` = '.$type.', `time` = '.$time.', `time_edit` = "0", `time_post` = '.$time.', `reason_close` = "", `id_user` = '.$user['id']);
            $insert = mysql_insert_id();
            if ($set['new_them_Location'] == 1) {
                header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$insert.'.html');
            } else {
                header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/');
            }
            exit;
        }
    } elseif (isset($_POST['cancel'])) {
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/');
        exit;
    }
    ?>
    <form action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/create_theme.html' method = 'post' class="p_m">
        <b>Название темы (<?= $set['forum_new_them_max_i'] ?> символ(а/ов)):</b><br />
        <input type = 'text' name = 'name' value = '' style = 'width: 96%' /><br /><br />
        <b>Содержание темы:</b><br />
        <textarea name = 'description' style = 'width: 96%'></textarea><br /><br />
        <?
        if ($user['group_access'] > 7) {
            ?>
            <b>Вывод темы в разделе:</b><br />
            <label><input type = 'checkbox' name = 'type' value = '1' /> Тема всегда вверху</label><br /><br />
            <?
        }
        ?>
        <input type = 'submit' name = 'create' value = 'Создать' /> <input type = 'submit' name = 'cancel' value = 'Отмена' />
    </form>
    <?
    include_once '../sys/inc/tfoot.php';
} else {
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/');
}
exit;

?>